FROM node:16

WORKDIR /app

COPY package*.json ./
RUN npm ci 

EXPOSE 3000

CMD ["node", "app.js"]  

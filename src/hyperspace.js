import { Server as HyperspaceServer, Client as HyperspaceClient } from 'hyperspace'
import Hyperbee from 'hyperbee'
import slugify from 'slug'

import * as config from './config.js'
import * as keys from './keys.js'

let server = undefined
let client = undefined
let store = undefined
let dbs = { local: undefined }


/* Initialize server and client, sets exported variables server and client */

export async function setup() {
	try {
		client = new HyperspaceClient()
		await client.ready()
	} catch (e) {
		// No Hyperspace daemon, start in process
		server = new HyperspaceServer({ storage: config.HYPERSPACE_STORAGE })
		await server.ready()
		console.log("Hyperspace daemon started.")

		client = new HyperspaceClient()
		await client.ready()
	}
	store = client.corestore(config.HYPERCORE_NAMESPACE)
	await store.ready()

	// Default corestore, with information about user and available core keys
	dbs.local = new Hyperbee(store.default(), {
		keyEncoding: 'utf-8',
		valueEncoding: 'json'
	})
	// We wait till it's ready and seed the local Hypercore
	await dbs.local.ready()
	await client.replicate( dbs.local.feed )

	console.log("Seeding:")
	console.log("   local: " + dbs.local.feed.key.toString('hex'))
	// We get all local guide-specific Hypercores
	const stream = await dbs.local.createReadStream(
		keys.streamOptions({ prefix: 'guide' })
	)
	for await (const entry of stream) {
		// Should we also seed remote cores? doing that now
		// TODO: Probably we should but we should do that at the end
		const guidedb = new Hyperbee( store.get({ key: entry.value.key }), {
			keyEncoding: 'utf-8',
			valueEncoding: 'json'
		})
		await guidedb.ready()
		// We don't want to block while we seed the cores
		// because the local ones replicate fast, but the remote
		// ones can take some time
		client.replicate( guidedb.feed ).then( () => {
			console.log(`   ${entry.key}: ${entry.value.key}`)
		} )
		dbs[ entry.key ] = guidedb
	}
}


/**
 * USER
 */

export async function setUserInfo( info ) {
	for ( const key in info ) {
		await dbs.local.put( key, info[ key ] )
	}
}

export async function getUserInfo() {
	const username = await dbs.local.get( 'username' )
	const emoji = await dbs.local.get( 'emoji' )
	const color = await dbs.local.get( 'color' )
	return {
		username: username ? username.value : 'anonymous',
		emoji: emoji ? emoji.value : config.DEFAULT_EMOJI,
		color: color ? color.value : config.DEFAULT_COLOR
	}
}

// User must have a username !== anonymous
export async function identified( req, res, next ) {
	const username = await dbs.local.get( 'username' )
	if ( !username || username === 'anonymous') {
		return res.status(401).json({ error: 'User must be identified' })
	}
	next()
}

/**
 * GUIDES
 */


/**
 * Create a new local guide with a name and a description, either for
 * a newly creaated guide or for an existing guide that has been joined and
 * need a writable local hypercore
 */
async function createLocalCore( slug, name, description, createdOn, createdBy, writer ) {
	// Create a new local hypercore
	const guidedb = new Hyperbee( store.get({ name: slug }), {
		keyEncoding: 'utf-8',
		valueEncoding: 'json'
	})
	await guidedb.ready()

	const metadata = {
		slug,
		name,
		description,
		createdOn,
		createdBy,
		writer
	}
	await guidedb.put('metadata', metadata)

	// Save guide name and key in local db
	const guideKey = `guide${ config.DIVIDER_GT }${ metadata.slug }${ config.DIVIDER_GT }`
	const key = guidedb.feed.key.toString('hex')
	await dbs.local.put( guideKey, { name, key } )

	// Save local guide db in the pool of open hypercores
	dbs[ guideKey ] = guidedb
	// Seed local guide hypercore
	console.log("Seeding:")
	console.log(`   ${guideKey}: ${key}`)
	await client.replicate( guidedb.feed )
}

export async function addGuide( name, description ) {
	// TODO check that slug isn't saved already, and if it is, add a number
	const slug = slugify( name )
	const username = await dbs.local.get( 'username' )
	const createdBy = username ? username.value : 'anonymous'

	// Create a local core with me as creator and writer
	return createLocalCore(
		slug,
		name,
		description,
		new Date(),
	 	createdBy,
		createdBy
	)
}

async function joinRemoteCore( key ) {
	// Get remote hyperbee
	const remotedb = new Hyperbee( store.get({ key }), {
		keyEncoding: 'utf-8',
		valueEncoding: 'json'
	})
	await remotedb.ready()
	await client.replicate( remotedb.feed )

	// Get hyperbee metadata
	// TODO add if cannot retrieve the value
	const metadataEntry = await remotedb.get( 'metadata' )
	const metadata = metadataEntry.value

	// Add remote guide key to local db
	// FIXME if there are two anonymous collaborators, this would overwrite the key of one of them
	const remoteKey = `guide${ config.DIVIDER_GT }${ metadata.slug }${ config.DIVIDER_GT }${ metadata.writer }`
	await dbs.local.put( remoteKey, {name: metadata.name, key} )

	// Log seeding
	console.log("Seeding:")
	console.log(`   ${remoteKey}: ${key}`)

	// Save remote db in the pool of open hypercores
	dbs[ remoteKey ] = remotedb
	return metadata
}

export async function joinGuide( key ) {
	const metadata = await joinRemoteCore( key )

	// Create local core with remote user as creator and me as writer
	const username = await dbs.local.get( 'username' )
	const writer = username ? username.value : 'anonymous'

	await createLocalCore(
		metadata.slug,
		metadata.name,
		metadata.description,
		metadata.createdOn,
		metadata.createdBy,
		writer
	)
}

export async function confirmGuide( key ) {
	const remoteMetadata = await joinRemoteCore( key )
	const slug = remoteMetadata.slug
	const writer = remoteMetadata.writer

	const localKey = `guide${ config.DIVIDER_GT }${ slug }${ config.DIVIDER_GT }`
	const metadataEntry = await dbs[ localKey ].get( 'metadata' )
	const metadata = metadataEntry.value

	metadata.contributors = metadata.contributors ? metadata.contributors : {}
	metadata.contributors[ writer ] = key

	await dbs[ localKey ].put( 'metadata', metadata )
}

export async function deleteGuide( slug ) {
	// Get all local and remote keys for this guide:
	const stream = await dbs.local.createReadStream(
		keys.streamOptions({ subkeysOf: ['guide', slug], includeKey: true })
	)
	const guideKeys = []
	const batch = dbs.local.batch()
	// For each guide-related key:
	for await (const entry of stream) {
		// Close and destroy its associated hypercore
		// FIXME: How can we destroy a whole hyperbee?
		// await dbs[ entry.key ].feed.destroyStorage()
		// Remove entry from local db
		await batch.del( entry.key )
		// Remove pointer from pool of open cores
		delete dbs[ entry.key ]
	}
	await batch.flush()
}

export async function editGuide( slug, name, description ) {
	const guideKey = `guide${ config.DIVIDER_GT }${ slug }${ config.DIVIDER_GT }`
	const metadata = await dbs[ guideKey ].get( 'metadata' )
	metadata.value.name = name
	metadata.value.description = description
	return dbs[ guideKey ].put( 'metadata', metadata.value )
}

/* Fetch my locally stored guides and retrieve their names, keys and metadata */
export async function getGuides() {
	const guides = []
	const inspected = []
	const stream = await dbs.local.createReadStream(
		keys.streamOptions({ prefix: 'guide' })
	)
	for await (const entry of stream) {
		const slug = entry.key.split('/')[1]
		// We make sure that we get guide info only once
		if (inspected.indexOf( slug ) === -1) {
			inspected.push( slug )
			guides.push( await getGuide(slug) )
		}
	}
	return guides
}

export async function getPublicKey( slug ) {
	// TODO catch db not found
	const guideKey = `guide${ config.DIVIDER_GT }${ slug }${ config.DIVIDER_GT }`
	// TODO check that key is from guide creator
	return dbs[ guideKey ].feed.key.toString('hex')
}

export async function getGuide( slug ) {
	// TODO catch db not found
	const guideKey = `guide${ config.DIVIDER_GT }${ slug }${ config.DIVIDER_GT }`
	const guide = await dbs[ guideKey ].get( 'metadata' )
	const metadata = guide.value
	const me = await dbs.local.get( 'username' )
	const username = me ? me.value : 'anonymous'

	// Initialize status and contributors
	metadata.status = config.JOIN_STATUS_CREATOR
	if (!metadata.contributors) {
		metadata.contributors = {}
	}

	// If I am not the creator of the guide, se the main db metadata
	if (username !== metadata.createdBy) {
		metadata.status = config.JOIN_STATUS_AWAITING

		// TODO catch db not found
		const mainKey = `guide${ config.DIVIDER_GT }${ slug }${ config.DIVIDER_GT }${ metadata.createdBy }`
		const mainMetadataEntry = await dbs[ mainKey ].get( 'metadata' )
		const mainMetadata = mainMetadataEntry.value

		// grab contributors and status
		if ( mainMetadata.contributors ) {
			metadata.contributors = mainMetadata.contributors
			if (metadata.contributors[ username ]) {
				metadata.status = config.JOIN_STATUS_CONTRIBUTOR
			}
		}
	}
	return metadata
}


/**
 * SPOTS
 */

async function addSpotToBee( bee, payload ) {
	// Insert the payload under a unique key
	const spotKey = 'spot' + config.DIVIDER_GT + keys.createId()
	await bee.put( spotKey, payload )

	// Insert pointers to that key with lexicographically comparable lats and lngs
	const latKey = 'lat' + config.DIVIDER_GT + keys.latToLex(payload.lat)
	const lngKey = 'lng' + config.DIVIDER_GT + keys.lngToLex(payload.lng)

	await bee.put( latKey, spotKey )
	await bee.put( lngKey, spotKey )

	// TODO: Insert pointers with tags and names
	// TODO: Maybe also use batch insert
	return spotKey
}

export async function addSpotToLocal( payload ) {
	return addSpotToBee( dbs.local, payload )
}

export async function addSpotToGuide( payload, slug ) {
	// We want our local hypercore:
	const guideKey = 'guide' + config.DIVIDER_GT + slug + config.DIVIDER_GT
	const entry = await dbs.local.get( guideKey )

	const core = store.get({ key: entry.value.key })
	const guidedb = new Hyperbee( core, {
		keyEncoding: 'utf-8',
		valueEncoding: 'json'
	})
	// TODO: Consider if we want to add pointers in the local db to guidedb spots
	return addSpotToBee( guidedb, payload )
}

/* Overwrites the whole spot value with the payload */
export async function editSpot( id, payload ) {
	const spotKey = 'spot' + config.DIVIDER_GT + id
	const oldSpot = await dbs.local.get( spotKey )

	// Check if lat or lng have changed, remove old ones and add new ones
	const deletePointers = []
	const addPointers = []
	if ( oldSpot.value.lat !== payload.lat ) {
		// Delete old lat pointer
		const oldLatKey = 'lat' + config.DIVIDER_GT + keys.latToLex(oldSpot.value.lat)
		const newLatKey = 'lat' + config.DIVIDER_GT + keys.latToLex(payload.lat)
		deletePointers.push(oldLatKey)
		addPointers.push(newLatKey)
	}
	if ( oldSpot.value.lng !== payload.lng ) {
		// Delete old lat pointer
		const oldLngKey = 'lng' + config.DIVIDER_GT + keys.lngToLex(oldSpot.value.lng)
		const newLngKey = 'lng' + config.DIVIDER_GT + keys.lngToLex(payload.lng)
		deletePointers.push(oldLngKey)
		addPointers.push(newLngKey)
	}

	// TODO: Check if tags have changed, remove old ones and add new ones

	const batch = dbs.local.batch()

	// Insert payload
	await batch.put( spotKey, payload )
	// Insert pointers
	for (const key of deletePointers) { await batch.del(key) }
	for (const key of addPointers) { await batch.put(key, spotKey) }

	await batch.flush()
}

export async function deleteSpot( id ) {
	const spotKey = 'spot' + config.DIVIDER_GT + id
	const oldSpot = await dbs.local.get( spotKey )

	// Delete lat/lng pointers
	const latKey = 'lat' + config.DIVIDER_GT + keys.latToLex(oldSpot.value.lat)
	const lngKey = 'lng' + config.DIVIDER_GT + keys.lngToLex(oldSpot.value.lng)

	// TODO: delete tag pointers

	const batch = dbs.local.batch()
	await	batch.del(spotKey)
	await	batch.del(latKey)
	await	batch.del(lngKey)
	await	batch.flush()
}


export async function getSpot( spotId ) {
	const spot = await dbs.local.get( 'spot' + config.DIVIDER_GT + spotId )
	/* TODO add spot serializer to use in all functions */
	return {
		id: spotId,
		data: spot.value
	}
}

async function getSpotsFromBee( bee ) {
	const stream = await bee.createReadStream(
		keys.streamOptions({ prefix: 'spot' })
	)
	const spots = []

	// TODO create a spot serializer
	for await (const entry of stream) {
		spots.push({
			id: entry.key.split(config.DIVIDER_GT)[1],
			data: entry.value
		})
	}
	return spots
}

export async function getLocalSpots() {
	return getSpotsFromBee( dbs.local )
}

export async function getGuideSpots( slug ) {
	// We want to get from all guide hypercores:
	// * Local/write hypercore for this guide: `guide/the-magpie/`
	// * Remote/read hypercores for this guide: `guide/the-magpie/username`
	// For this we need to search from 'guide/slug/' to 'guide/slug0'

	// NOTE: instead of searching in localdb, because we already have all
	// hypercores open, we could just search over the keys of the dbs pool
	const stream = await dbs.local.createReadStream(
		keys.streamOptions({ subkeysOf: ['guide', slug], includeKey: true })
	)
	const guideSpots = []
	for await (const entry of stream) {
		if (dbs[ entry.key ]) {
			const spots = await getSpotsFromBee( dbs[ entry.key ] )
			spots.map(function(s) {
				guideSpots.push(s)
			})
		} else {
			// TODO what happens when it's not available? Can it happen?
			console.log("Hyperbee " + entry.key +" NOT AVAILABLE")
		}
	}
	return guideSpots
}


// TODO add guide parameter to bound search
export async function getBoundSpots( bounds, slug = null ) {
	let stream = undefined

	/* Search all keys bound by lat */
	const xlat = keys.latToLex( bounds.xlat )
	const ylat = keys.latToLex( bounds.ylat )

	stream = await dbs.local.createReadStream(
		keys.streamOptions({ prefix: 'lat', gte: xlat, lte: ylat })
	)

	const bylat = []
	for await (const entry of stream) {
		bylat.push( entry.value )
	}

	/* Search all keys bound by lng */
	const xlng = keys.lngToLex( bounds.xlng )
	const ylng = keys.lngToLex( bounds.ylng )

	stream = await dbs.local.createReadStream(
		keys.streamOptions({ prefix: 'lng', gte: xlng, lte: ylng })
	)

	const bylng = []
	for await (const entry of stream) {
		bylng.push( entry.value )
	}

	const foundKeys = bylat.filter( value => bylng.includes( value ) )

	const spots = []
	for (const key of foundKeys) {
		const spot = await dbs.local.get(key)
		spots.push( spot )
	}
	return spots
}



/* Creates a new Hypercore and Hyperbee database with the given store name */
export async function newStore(storeName ) {
	/* TODO: create in a local database a name-key entry */
	const core = client.corestore().get({
		name: storeName
	})
	/* FIXME: probably we don't need to create the Hyperbee here */
	const bee = new Hyperbee( core, {
		keyEncoding: 'utf-8',
		valueEncoding: 'json'
	})
	await core.ready()
	return core.key.toString('hex')
}


export async function cleanup() {
	console.log('Shutting down:')
	for (const db in dbs) {
		try {
			await dbs[db].feed.close()
			console.log(`    Hyperbee ${db}... DONE`)
		} catch (e) {
			console.log(`    Hyperbee ${db}... FAILED`)
		}
	}

	if (client) {
		await client.close()
		console.log('    Hyperspace client... DONE')
	}
	if (server) {
		server.close()
		console.log('    Hyperspace daemon... DONE')
	}
}


/* TEST METHOD, print all local hyperbee entries */
export async function inspect() {
	for (const key in dbs) {
		console.log(`\n\nInspecting core ${key}\n`)
		const stream = await dbs[ key ].createReadStream()
		for await (const entry of stream) {
			console.log(entry)
		}
	}
}


import mlts from 'monotonic-lexicographic-timestamp'

import * as config from './config.js'

export const createId = mlts()

/**
 * Create the options object to send to hyperbee.createReadStream
 */
export function streamOptions( payload ) {
	const opts = {}
	// Set up gte or lte if specified
	if ( payload.gte ) opts.gte = payload.gte
	if ( payload.lte ) opts.lte = payload.lte
	// Set up or prepend prefix if specified
	if ( payload.prefix ) {
		opts.gte = payload.prefix + config.DIVIDER_GT + ( opts.gte || '' )
		opts.lte = payload.prefix + config.DIVIDER_LT + ( opts.lte || '' )
	}
	// Get key and all subkeys
	if ( payload.subkeysOf ) {
		const key = payload.subkeysOf.join( config.DIVIDER_GT )
		if ( payload.includeKey ) opts.gte = key + config.DIVIDER_GT
		else opts.gt = key + config.DIVIDER_GT
		opts.lte = key + config.DIVIDER_LT
	}
	return opts
}

/**
 * Latitude is between -90 and 90 and we can receive 7 decimals
 * Coordinate to lexicographically comparable string.
 * Applies the following transformations:
 * - lat * 1e7
 * - to integer (remove decimals)
 * - normalize to positive (add max*1e7)
 * - to string
 * - left pad to 10 characters
 */
function coordToLex(coord, max) {
	let n = Math.floor(coord * 1e7)
	n += (max * 1e7)
	let s = n.toString()
	let pad = '0000000000' // 10 digits
	return (pad + s).slice(-pad.length)
}

/**
 * Lexicographically comparable string to coordinate.
 * Applies the following transformations:
 * - string to integer
 * - to negative (subtract max*1e7)
 * - to float, divide by 1e7
 */
function lexToCoord(lex, max) {
	let n = parseInt(lex, 10)
	n -= (max * 1e7)
	return n / 1e7
}

/**
 * Calls coordToLex where the max value of the latitude is 90
 */
export function latToLex(lat) {
	return coordToLex(lat, 90)
}

/**
 * Calls coordToLex where the max value of the latitude is 90
 */
export function lexToLat(lex) {
	return lexToCoord(lex, 90)
}

/**
 * Calls coordToLex where the max value of the lngitude is 180
 */
export function lngToLex(lat) {
	return coordToLex(lat, 180)
}

/**
 * Calls lexToCoord where the max value of the lngitude is 180
 */
export function lexToLng(lex) {
	return lexToCoord(lex, 180)
}



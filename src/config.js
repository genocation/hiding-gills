export const HYPERSPACE_STORAGE = process.env.LOCAL_STORAGE || './local_storage'
export const HYPERSPACE_NAME = 'my-hiding-gills'
export const HYPERCORE_NAMESPACE = 'hiding-cores'

export const DIVIDER_GT = '/'
export const DIVIDER_LT = '0'

export const DEFAULT_EMOJI = '&#127812'
export const DEFAULT_COLOR = '#c1fcac'

export const JOIN_STATUS_CREATOR = 'Main'
export const JOIN_STATUS_AWAITING = 'Awaiting'
export const JOIN_STATUS_CONTRIBUTOR = 'Contributor'

# The Hiding Gills

A P2P mushroom forager's map using [Hypercore Protocol](https://hypercore-protocol.org/), a
  decentralized experiment for Wikimedia's Inspiration Week.

![app](./assets/images/app.png)


## Requirements

* nodejs >= v14.0.0
* npm

## Getting Started

To start the project, install the dependencies with npm:

```
npm install
```

And run the app with:
```
node app.js
```

## Add content

Open another terminal to test the APIs and load some data on your local Hypercore:

You can set your user data with:

```
# Set your username:
curl "http://localhost:5000/api/user/set?username=<USERNAME>"

# You can also set an emoji and your profile color:
curl "http://localhost:5000/api/user/set?emoji=%26%23<HTML_EMOJI_CODE>&color=%23<COLOR_HEX_CODE>"

# For example:
curl "http://localhost:5000/api/user/set?username=genocation&emoji=%26%23127825&color=%23c1fcac"
```

Add a mushroom spot 🍄:

```
curl -X POST -d '{"lat":42.9453986, "lng":-2.7196453, "tags":["cep", "penny bun", "porcini"], "species": "Boletus Edulis", "observations":{ "description": "Cep next to a path!" }}' -H "Content-Type: application/json" http://localhost:5000/api/spots/add/
```

You can check your user information with:
```
curl "http://localhost:5000/api/user/"
```

And query mushroom spots saved locally in your Hypercore with:
```
# Get all spots saved locally
curl "http://localhost:5000/api/spots/local"

# Or get spots but filter by bounds
curl "http://localhost:5000/api/spots/local?xlat=41.95&ylat=43.97&xlng=-2.8&ylng=-2.5"
```


## Run docker container

```
# Build
COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose build

docker-compose up
```

(function() {

	var mymap = null;
	var markers = []

	function start() {
		// Berrikano:
		var [ lat, lng ] = [ 42.9453986, -2.7196453 ]
		mymap = L.map('mapid').setView([lat, lng], 16)

		var apiKey = '06bd6d43e61c40da82c98332b280e9c6'
		var tileUrl = `https://tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=${apiKey}`

		L.tileLayer( tileUrl , {
			maxZoom: 18,
			attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> | <a href="https://www.thunderforest.com/maps/outdoors/">Thunderforest</a>',
		}).addTo(mymap)

		if (thisGuide) {
			fetchGuideSpots( thisGuide.slug )
		} else {
			fetchLocalSpots()
		}
	}

	function getRandomIcon() {
		var key = 'mushy' + (Math.floor(Math.random() * 4) + 1)
		return icons[ key ]
	}

	function capitalizeFirstLetter( word ) {
		const lower = word.toLowerCase()
		return lower.charAt(0).toUpperCase() + lower.slice(1)
	}

	function buildWikipediaLink( title ) {
		return 'https://en.wikipedia.org/wiki/' + capitalizeFirstLetter(title).split(' ').join('_')
	}

	function buildTagList( tags ) {
		if (!tags || tags.length === 0) return ''
		return '<ul class="tags">' + tags.map( tag => `<li>${ tag }</li>` ).join('') + '</ul>'
	}

	function buildGuideList( guides ) {
		if (!guides || guides.length === 0) return ''
		return '<ul class="guides">' + guides.map( guide => `<li><a href="#">${ guide }</a></li>` ).join('') + '</ul>'
	}

	function buildObservationList( observations ) {
		const list = []
		for (const obs in observations) {
			list.push( `<p><strong>${ capitalizeFirstLetter(obs) }:</strong> <span>${ observations[obs] }</span></p>`)
		}
		return list.join('')
	}

	function renderSpots( spots ) {
		const bounds = []
		for (const spot of spots) {
			// Create marker
			const marker = L.marker([ spot.data.lat, spot.data.lng ], { icon: getRandomIcon() }).addTo(mymap)
			// Bind marker to popup
			const popup = `
					<div>
					<h2><a href="${ buildWikipediaLink(spot.data.species) }" target="_blank">${ spot.data.species }</a></h2>
					<div>${ buildTagList(spot.data.tags )}</div>
					<p>${ buildObservationList(spot.data.observations) }</p>
					<div>${ buildGuideList(spot.data.guides) }</div>
					</div>
				`
			marker.bindPopup( popup )
			markers.push( marker )
			bounds.push([ spot.data.lat, spot.data.lng ])
		}
		// Fit map to marker bounds if any
		if (bounds.length > 0) {
			mymap.fitBounds( bounds, {padding: [10,10] })
		}
	}

	function fetchLocalSpots() {
		axios.get('/api/spots/local').then( (response) => renderSpots(response.data) )
	}

	function fetchGuideSpots( slug ) {
		axios.get('/api/guides/' + slug + '/spots' ).then( (response) => renderSpots(response.data) )
	}

	var icons = {
		mushy1:  L.icon({
			iconUrl: '/images/1.png',
			shadowUrl: '/images/1s.png',
			iconSize:     [64, 64],  // size of the icon
			shadowSize:   [100, 64], // size of the shadow
			iconAnchor:   [32, 64],  // point of the icon which will correspond to marker's location
			shadowAnchor: [32, 64],  // the same for the shadow
			popupAnchor:  [-3, -76]  // point from which the popup should open relative to the iconAnchor
		}),
		mushy2: L.icon({
			iconUrl: '/images/2.png',
			shadowUrl: '/images/2s.png',
			iconSize:     [64, 64],  // size of the icon
			shadowSize:   [100, 64], // size of the shadow
			iconAnchor:   [32, 64],  // point of the icon which will correspond to marker's location
			shadowAnchor: [32, 64],  // the same for the shadow
			popupAnchor:  [-3, -76]  // point from which the popup should open relative to the iconAnchor
		}),
		mushy3: L.icon({
			iconUrl: '/images/3.png',
			shadowUrl: '/images/3s.png',
			iconSize:     [64, 64],  // size of the icon
			shadowSize:   [100, 64], // size of the shadow
			iconAnchor:   [32, 64],  // point of the icon which will correspond to marker's location
			shadowAnchor: [32, 64],  // the same for the shadow
			popupAnchor:  [-3, -76]  // point from which the popup should open relative to the iconAnchor
		}),
		mushy4: L.icon({
			iconUrl: '/images/4.png',
			shadowUrl: '/images/4s.png',
			iconSize:     [64, 64],  // size of the icon
			shadowSize:   [100, 64], // size of the shadow
			iconAnchor:   [32, 64],  // point of the icon which will correspond to marker's location
			shadowAnchor: [32, 64],  // the same for the shadow
			popupAnchor:  [-3, -76]  // point from which the popup should open relative to the iconAnchor
		})
	}

	start();
})();

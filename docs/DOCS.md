# Week of 17th May 2021: Inspiration Week!

[TOC]

## Monday, 17th May 2021

### Mapeo: Digital Democracy

* User guide: https://docs.mapeo.app/
* Digital democracy Github: https://github.com/digidem/
* Peermaps: https://peermaps.org/
* Cooperative Ownership of Data without Blockchain: https://www.digital-democracy.org/blog/blockchain/
* Local first: https://www.digital-democracy.org/blog/localfirst/

> Blockchain takes peer-to-peer technology and adds an algorithm called “machine-facilitated
> consensus,” which can be relatively complex. This programmatic consensus involves actors called
> “miners” who run computationally expensive algorithms that earn money in exchange for computing
> power. These miners are sometimes able to make profit over their electricity costs. This
> incentivizes the purchase of ever larger and more expensive data centers, rewarding miners who are
> able to take larger financial risks. This model at best excludes those living in poverty, and at
> worst, extorts them. What’s been shown is that with blockchain mining, the rich get richer, the poor
> get poorer, and those in the middle take enormous risks that can go either way. – and that’s not
> even factoring in the negative environmental impacts.


* No, Everyone is Not Getting Rich Off Bitcoin: https://www.investopedia.com/news/no-everyone-not-getting-rich-bitcoin/
* Environmental cost of cryptocurrency mines: https://www.sciencedaily.com/releases/2019/11/191113092600.htm

> But why does a blockchain need this complex consensus algorithm? Because this system is designed for
> a scenario where public transactions are mediated between participants which are all potentially
> malicious. These “trustless” transactions are the key assumption baked within blockchains that
> distinguishes them from peer-to-peer applications.


> Although blockchain authors claim it to be ‘trustless’ and horizontal and autonomous, technology is
> not neutral, which in practice means that you have to trust someone at some point. Any technology
> requires its users to trust the rules set by the software engineers, designers, and investors. Those
> able to understand and participate in these technical rulemakings primarily represent existing power
> imbalances – they are mainly technical, white, and male, and largely from the U.S. and Europe. So
> then, is a “trustless” public blockchain that much of a departure from the centralized power
> structures which we are trying to move away from? Who has the incentive to participate and invest in
> a blockchain to set the rules? Who benefits when a new market is created and who is enriched by that
> market?

> Blockchain simply isn’t the right tool for a privacy-first digital knowledge commons. It isn’t the
> right tool for challenging the tech colonialist status quo of Silicon Valley. To be clear, there may
> be some important uses for it, but we have concluded it’s not well-suited for protecting freedom of
> expression and human rights

Read book: "Understanding Knowledge as a Commons" https://mitpress.mit.edu/books/understanding-knowledge-commons


### Mapeo Architecture

* Database: KappaDB
* Replication: Multifeed


### Dat

Awesome Dat: https://awesome.datproject.org/


### Hypercore

https://hypercore-protocol.org/

hyperdrive - secure, decentralized peer-to-peer file system on top of hypercore
* https://github.com/mafintosh/hyperdrive
hypercore - decentralized peer-to-peer append-only logs using hypercore protocol
* https://github.com/mafintosh/hypercore
Hyper SDK:
* https://github.com/datproject/sdk


### OpenStreetMap

OpenStreetMap Developer tools:
https://wiki.openstreetmap.org/wiki/Develop

Osmium:
Osmium (also known as Libosmium) is a Swiss Army knife for processing OpenStreetMap data. It is a library to read and write OpenStreetMap data and serves as a base for other projects such as Osm2pgsql. 

Osmconvert:
https://wiki.openstreetmap.org/wiki/Osmconvert#Download

Planet.osm: OSM data in one file
https://wiki.openstreetmap.org/wiki/Planet.osm


### Leaflet

Javascript library to work with OSM https://leafletjs.com/


### Kappa OSM
https://github.com/digidem/kappa-osm



### Let's use Hypercore SDK for something, not sure about what yet

Hyperspace: server that bundles many of core modules.

Using Hyperspace: https://hypercore-protocol.org/guides/getting-started/hyperspace/

Hypercore:  Hypercore is a secure, distributed append-only log.

RemoteHypercore: https://hypercore-protocol.org/guides/hyperspace/hypercore/

QUESTION: What's the difference between RemoteHypercore and Hypercore

RemoteCorestore to create RemoteHypercore. A Corestore is a Hypercore factory, provides a get method
to create or instantiate Hypercores.

`HyperspaceClient().corestore()` -> returns a corestore

`corestore.get()` -> returns a Hypercore.

`HyperspaClient().replicate()` -> starts seeding Hypercore to the Hyperswarm network

Hyperswarm is the Hypercore network:

`HyperspaceClient().replicate()` is sugar around:

```JavaScript
client.network.configure(core.discoveryKey, { announce: true, lookup: true })
```

DHT: Distributed Hash Table


### Hypercore protocol workshop: part 1

https://github.com/hypercore-protocol/p2p-indexing-and-search/blob/main/problems/

#### 0 Welcome

About *Hypercores*:
* A Hypercore can only be appended to by its original creator, the writer. The writer maintains a private key which they use to sign a new proof each time data is appended to the Hypercore.
* The writer's public key is used as a unique, global identifier for the Hypercore.

About *Hyperswarm*:
* sharing and discovery system
* options:
  * announce: advertise to the DHT that you are in posession of the corresponding Hypercore
  * lookup: will not insert new entries into de DHT, only queries DHT to discover other peers
* using Hyperswarm, you can ask the global network "Who has the Hypercore with key X?" and Hyperswarm will reply with peers you can connect to and replicate from.


#### Poblem 1: getting started

Install hyperspace CLI:

```
npm install -g hyperspace
```

We need hyperspace also installed in the development folder to be able to do:

```JavaScript
const { Client } = require('hyperspace')

start()

async function start () {
  const c = new Client()

  // Ask for the RPC server status to see that everything works
  console.log( await c.status() )
}
```


#### Problem 2: hypercore basics

Concepts:
* Hyperspace's corestore: Creates a new Corestore instance.
* Corestore's get: Get a Hypercore, or create a new one (if no options are passed in).
* Hypercore's ready: Make sure the Hypercore's state (it's key, for example) is loaded.
* Hypercore's append: Append a new block, or an array of blocks, to a Hypercore.
* Hypercore's get: Get the block at a specific index.

Interactinv with an append-only log:
* Knowing what the public key of the log is so you can share it with other people.
* Appending a new block of data to the log (core.append).
* Getting a block of data from an index (core.get).
* Knowing how many blocks of data are in the log (core.length).


#### Problem 3: appending things

Get stats from `process-top` and append it in the core.

Also, core emits an `append` event which we can catch:
```JavaScript
core.on('append', async function() {
  block = await core.get(i)
});
```

Still, we are only one person appending things to our own log.


#### Problem 4: basic replication

Sharing the stats with another peer. Each core is identified by a globally unique public key. We can
share the cores with other people by sharing the public key.

Here we are going to simulate replication by creating another client for the current hyperspace
server. Use

```JavaScript
store.get( key, { options … })
```

**Let's replicate with other Peers!**

Hyperspace exposes a function to share Hypercores over our P2P network: `replicate`

If we have another computer we can test it with it, but if we don't we can use the hyperspace
simulator:

```
hyperspace-simulator script.js
```

BUT WTF let's do it with another computer!!!!!


1. Install hyperspace globally
2. Run hyperspace -s 'workshop-data'
3. Run reader with the hypercore key from the writer core


**IT FUCKING WORKS!!!!!!**


#### Problem 5: querying

Hypercore download blocks you're accessing on-demand, as requested: **sparse syncing**.

Stuff:
* Hypercore's download event: Emitted whenever a block is downloaded from a remote peer.
* https://github.com/hypercore-protocol/hypercore#feedondownload-index-data

If we iterate over all the blocks till we find what we want, we are downloading everything. Takes
ages.

Results with:
* Naive search:
  * Downloaded blocks: 2160
  * Time in ms: 20716
* Binary search:
  * Downloaded blocks: 11
  * Time in ms: 99


#### Problem 6: scaling things up

We can't, the address that they give in the exercise doesn't have any peers!:

```
RemoteHypercore(
  key: 309ba1736cc5af2940fc9fb03d256547b78ca9972ce4398648a7d7782d31ad4b
  discoveryKey: a33d9da0369f1d04200b81b815d4e4e1f03086532d5555aca5623fddbb682c3b
  opened: true
  writable: false
  length: 0
  byteLength: 0
  peers: 0
)
```

## Tuesday, 18th May 2021

### Hypercore protocol workshop: part 2

#### Problem 7: append-only data structures

Embedded indexes: info that we add to log entries that acts as routing info to find entries as
efficiently as possible.

Tricky stuff, tradeoff:
* Small enough not to take up too much space in the log entry
* But powerful enough to support finding what you are looking for efficiently.


#### Problem 8: remote large database

Will not work because there are no peers, so cannot connect to the fantastic germany core


#### Problem 9: a simple database

We can design indexes to contain whatever we want in order to facilitate our own search. 

From the exercise text:

> But first for a bit of terminology: what you built in the previous exercises is a limited form of
  a "key/value store", or "kv-store" for short. Time is the key, and your process statistics are the
  values. There are two basic operations here: _put_ a timestamp/stats pair, and _get_ the stats for a
  given timestamp.

For example, instead of the timestamp, we can have a key with a dictionary word. Let's try this
with:

```
const { kvPairs } = require('websters-english-dictionary')
```

#### Problem 10: using hyperbee

Now we build up to understand the database: **Hyperbee**:
* An append only Btree running on a Hypercore. Allows sorted iteration and more
* https://github.com/hypercore-protocol/hyperbee
* Hyperbee is P2P key/value store build on top of a Hypercore using an embedded index that projects
a B-tree to find and store data.

More about hyperbee:

> That basically means we get to store key/value pairs efficiently where each put(key, value) just
> corresponds to a small append to the underlying Hypercore with the key and value plus some routing
> information that allows a reader to find any keys stored already in the log efficiently.
>
> It also exposes a sorted iterator API that allows you to use the embedded index to efficiently find
> ranges between two keys, which is useful when you want to build generic databases with Hyperbee as a
> foundation.


Once we install hyperbee:

```
npm install hyperbee
```

* https://github.com/hypercore-protocol/hyperbee#await-dbputkey-value


#### Problem 11: complex queries with hyperbee

> Luckily for us Key/Value stores usually support a "range query" interface that allows you to find
  all values between two keys in a sorted fashion. Range queries are super powerful -- in fact, the
  complex queries you run in modern database systems, like SQL, can be translated into a series of
  simple KV range queries.
> 
> Hyperbee provides a range query interface through the createReadStream method, which takes a
  handful of options for defining the bounds of the query, and gives you a result stream back.
  Here's an example:


```JavaScript
for await (const data of db.createReadStream({ gte: '>=-to-this-key', lt: '<-than-this-key' })) {
  console.log(data)
}
```

#### Problem 12: secondary indexing

This means adding another entry to the database with a different index, so for example if the data
is `key: value` but we also want to search by timestamp, we can have a duplicate of the entry being
`timestamp/key: value`, which allows us to use the `createReadStream` options `gt` or `lt`, etc, to
query blocks without having to download the whole log.

This means that the database can become HUGE, but sparse syncing means that we will only be
downloading what we need:

> Secondary indexing is essential for supporting different types of queries. Each index is usually
> tailored to a particular set of query types, so the more secondary indexes you add, the more types
> of queries you can answer quickly. This requires a lot of space though -- the more secondary indexes
> you add, the larger your database becomes.
> 
> Fortunately for us, sparse syncing means that even if the database is packed full of secondary
> indexes (huge!), readers will still only download the bits they need, when they need them.


#### Problem 13: key encodings

Just make sure that the key encoding is correct and allows you to compare with gt and lt. For
example, the comparison between the strings '100' and '20' is problematic because '100' is smaller
than '20'.

We can fix this by encoding these values in hexadecimal, which we can do with packages like:

```
npm install lexicographic-integer
```


#### Problem 14:

We should be able to query a database with all the IMDB titles, with the following indices, but it's
not available because reasons.

```
ids!{imdbTitleId} -> metadataObject
```

Ratings:

```
ratings!{lexint.pack(10 * rating, 'hex')}!${lexint.pack(ratingVotes, 'hex')}!${imdbTitleId} -> imdbTitleId
```

For example, for title `tt8760684` with rating `8.2` and `20736` votes:

```
ratings!{lexint.pack(82, 'hex')}!${lexint.pack(20736, 'hex')}!tt8760684

ratings!52!fc5005!tt8760684
```

And another indes for keywords:

```
keywords!{keyword.toLowerCase()}!lexint.pack(10 * rating, 'hex')!${imdbTitleId} -> imdbTitleId
```


## Wednesday,19th May 2021

The same way that we run the daemon with `hyperspace -s 'workspace-data'` we can do it
programatically:

```JavaScript
const server = new HyperspaceServer({
  storage: './my-hyperspace-storage',
  host: 'my-hyperspace'
})
```

We can also install the Hyp CLI:

```
npm install -g @hyperspace/cli
```

Instead of running `hyperspace -s 'workspace-data'` we can use the cli doing `hyp daemon start` and `hyp daemon stop`

https://hypercore-protocol.org/guides/hyp/
Demo video about Hyp: https://www.youtube.com/watch?v=SVk1uIQxOO8

Some nice demo about hyp:

```
# Create drive
$ hyp create drive

hyper://xxx

# Inspect the drive
$ hyp ls hyper://xxx

# Write in the drive
$ hyp put hyper://xxx/hello.txt "Hello world!"

$ hyp cat hyper://xxx/hello.txt

$ hyp info  # logs what's happening
```

From a remote computer we can do the same: behind the scenes, when I do `hyp ls` or `hyp cat` the
two computers find each other using a DHT and stream the information in real time.

```
# I can sync the files from another computer so that we can always access it.
$ hyp sync hyper://xxx ./destination/path

# If we leave the sync command running, when we update the files, it is directly synced over the
network to keep the files updated.
```


#### Hyperdrive and Hyperbee app workshop:

Followed the youtube stream about coding a Hyperbee and Hyperdrive app, with Paul Frazee:

https://www.youtube.com/watch?v=Y_8o6BlRF6s&t=3199s


#### Dat-sdk worskhop

https://github.com/RangerMauve/dat-workshop/


#### Some stuff about the Hypedivision community

About the power of decentralization and how moderation is an important issue to solve:

From
https://medium.com/@paulfrazee/the-anti-parler-principles-for-decentralized-social-networking-80a490909b38
by Paul Frazee

> When people look at Parler or Trump being booted from the Internet, they might say that’s the
> concerning show of monopoly power. It is, but that’s not actually what concerns me most. What
> concerns me is the ten years that led up to this point when platforms *didn’t* make those choices
> because they felt they had to remain unbiased.
> 
> It’s not a problem of too much moderation, but too little. It led us to say “I don’t want Mark
> Zuckerberg to decide what is true,” because we know that would be biased toward Facebook’s interests
> as a company, not biased toward the interests of users. But that doesn’t mean we don’t need
> moderation. It means we need moderation that’s biased toward the interest of users.
> 
> Decentralization is about authority: who has it, and how can they exercise it.
> 
> It’s not about removing authority; the idea is a fiction. When users have abilities to modify a
> shared space, then that’s an authority model.

More, about individual vs collective rights:

> Decentralization trends toward libertarian ideals, but collective rights are as important as
> individual rights. Shared spaces must have ways to moderate, to decide how and what goes viral,
> whether somebody can be banned, and even details about how the software is built (how many
> characters are allowed in a post, or whether the community has a like button).
> 
> When we’re talking about somebody’s own data — their posts, their social relationships, perhaps
> their identity — we’re talking about their rights as an individual. When we’re talking about shared
> data — posts visible in a forum, the assignment of usernames, the ranking of search results — we’re
> talking about everyone’s collective rights.

Paul Frazee, founder of Secure Scuttlebutt, works on Beaker Browser, on Hypercore protocol and is
developing CTZN, a decentralized social network similar to twitter feeds:

https://ctznry.com/pfrazee@ctzn.one/feed

Values of decentralization talk in FOSDEM 2021
https://www.youtube.com/watch?v=ULFj714_Vvo



#### CTZN as an example:

Interesting stuff about CTZN:
* Uses hyperbee and hyperspace.
* Reading about the Architecture https://github.com/bluelinklabs/ctzn/blob/master/docs/design.md
* Database schemas:
  * Schema gide: https://github.com/bluelinklabs/ctzn/blob/master/docs/schemas.md
  * All schemas: https://ctzn.network/schemas


#### Mushroom mapping app: name brainstorming

Mushroom related terms:
* gills
* mycology
* mycelium
* mycorrhiza
* spores
* fungimap
* mold
* mycota
* fungal
* mushroom
* -take (japanese)
* wild
* forest
* shroom
* toadstool
* soil
* underground
* hyphae

Map or wiki related terms: 
* geo-
* loc-
* carto-
* map-
* wiki
* -pedia
* hunt
* find
* places
* foraging
* hide
* seek
* atlas
* globe
* graph
* plot
* tracer
* topo-
* earth

Ideas:
* Geomycota 🍄
* Geomold
* Geofungal 
* Geofungi 🍄
* Geoshroom
* Geomycelium
* Mycomaps
* Mycoloc
* Mycohunt 🍄
* Mycoplot
* Wikimycota
* Fungiloc
* Fungihunt
* Fungimaps
* Cartofungus
* Cartomycelium
* Cartomycota
* Cartoshrooms
* Cartomold
* Moldhunt
* Gillhunter
* Foragers 🍄
* Hiding gills 🍄
* Forest mapper
* Moldhunters 🍄
* Moldmapper
* Mycoglob
* The Undermap
* Soilmap
* Moldmap


### 🍄 The Hiding Gills 🍄

OpenStreetMaps:
* Wiki: https://wiki.openstreetmap.org/wiki/Main_Page

Leaflet: 
* Docs https://leafletjs.com/reference-1.7.1.html

Thunderforest Outdoors map tiles:
* https://www.thunderforest.com/maps/outdoors/
* API key: `06bd6d43e61c40da82c98332b280e9c6`
  * registered with genoff@ and Thunder<>
 

**Project Plan:**
* Data structure: `find` or `finding`
  * Attributes:
    * coordinates
    * specimen
    * tags or common names: e.g. The specimen might be `Boletus Edulis` but we need to be able to search `cep`
    * observations (JSON)
  * Search by:
    * coordinates (bounds)
    * species
    * tags
* Client:
  * Web app with map and access to hypercores
    * v0.0
      * Hardcoded array of hypercore keys
      * Get all `finds` for given geographic bounds
    * v0.1
      * Add user auth
      * Allow users to discover and sync different keys
      * Allow users to create new cores
* Server:
  * Hyperspace daemon 


Thoughts:
* I create a new hypercore:
* This is created locally
* **API: post create** returns key?
* I can write: add new `finds` to my "selected" hypercore key
* **API: post put find** giving all necessary details of the data structure
* I can read: search and display the `finds` in the map
* **API: get** return array of finds giving a search parameters
* Things are of course saved locally
* I open a new hypercore:
* **API: post open** given key
* I can read


My stuff:
* User creates a hypercore using a name:
  * user needs to save the name and the hypercore key
 

## Thursday, 20 May 2021

DHT to lookup peers

**Private Entities**:
- `user`:
  - has guides
  - attributes:
    - `nickname`

- `guide`:
  - consists on a set of spots where mushroom can be found
  - users share their guides with each others
  - attributes:
    - `name`
   
- `feed`?
  - aggregation of two users guides
  - has a name

- `spot`:
  - consists on a place where a mushroom is found:
  - attributes:
    - `coordinates`:number[] or (`lat`:number, `long`:number)
    - `specimen`:string
    - `tags`:string[]
    - `observations`:json


### Manage hypercores

Corestore API:
* https://github.com/hypercore-protocol/corestore

This module is the canonical implementation of the "corestore" interface, which exposes a Hypercore
factory and a set of associated functions for managing generated Hypercores.

Once we have a corestore, hypercores can be generated with get and default methods.

```
feed = corestore.get([key])
```

If the first writable core is created with `default`, it will be used for storage bootstrapping. It
can always be reloaded:

> We can always reload this bootstrapping core off disk without your having to store its public key
externally. Keys for other hypercores should either be stored externally, or referenced from within
the default core:

For the default corestore to be always the same one, we need to create the corestore using a
namespace!

```
const store = client.corestore(config.HYPERCORE_NAMESPACE)
wait store.ready()
```


### Local database

As we initialize the app, we create the default hyperbee with the following data:
```
# Information about the user
username: <local username>

# Keys for the locally saved guides
guide/<guide_name>: <guide_key>
```

And for each guide, we will have a hyperbee with:
```
metadata: { name: <guide_name>, description: <guide_description> }

# Information of the spots
spot/

# Information about the mushrooms?
mushroom/
```

### How to search?

We are using the composite keys to enhance search. For example, if we want to get all the guide keys
from our local database, because they are all prefixed with `guide/` and `0` is the next character
to `/`, we can do:

```
db.createReadStream({
  gte: 'guide/',
  lte: 'guide0'
})
```

For cleanness, we are going to save the delimiters in a constant so that we can do:

```JavaScript
async function getPrefixedResults( prefix ) {
  return db.createReadStream({
    gte: prefix + DELIMITER_GT,
    lte: prefix + DELIMITER_LT
  })
}
```

### Templating

Using Liquidjs: https://liquidjs.com/


### API examples

Set user information:
```
curl "http://localhost:5000/api/user/set?username=genocation&emoji=%26%23127825&color=%23c1fcac"
```

Get user information:
```
curl "http://localhost:5000/api/user/"
```

Get names, keys and metadata of my guides:
```
curl "http://localhost:5000/api/guides/"
```

Add a spot:
```
# Berrikano
curl -X POST -d '{"lat":42.9453986, "lng":-2.7196453, "tags":["cep", "penny bun", "porcini"], "species": "Boletus Edulis", "observations":{ "description": "Cep next to a path!" }}' -H "Content-Type: application/json" http://localhost:5000/api/spots/add/
# Gopegui
curl -X POST -d '{"lat":42.96, "lng":-2.728, "tags":["chantarelles", "yellow foot"], "species": "Cantharelllus lutescens", "observations":{ "description": "Little pine patch with yellow foot chantarelles from December to nearly February" }}' -H "Content-Type: application/json" http://localhost:5000/api/spots/add/
curl -X POST -d '{"lat":42.951178, "lng":-2.735086, "tags":["chantarelles", "yellow foot"], "species": "Cantharelllus lutescens", "observations":{ "description": "Loads of chanties every year" }}' -H "Content-Type: application/json" http://localhost:5000/api/spots/add/
```

Get all local spots:
```
# Get all local
curl "http://localhost:5000/api/spots/local"

# Get all local (with bounds)
## Returns only Chantarelle spot
curl "http://localhost:5000/api/spots/local?xlat=42.95&ylat=42.97&xlng=-2.8&ylng=-2.5"
## Returns Chantarelle and Bolete spot
curl "http://localhost:5000/api/spots/local?xlat=41.95&ylat=43.97&xlng=-2.8&ylng=-2.5"
```


### How to compare coordinates?


> The latitude must be a number between -90 and 90 and the longitude between -180 and 180.


**longitude (between -180 and 180)**

upper limit:
180 * 1e7 = 1800000000 + 180*1e7 = 3600000000

mid limit:
0 * 1e7 = 10000000 + 180*1e7 = 1810000000

lower limit:
-180 * 1e7 = -1800000000 + 180*1e7 = 0


**latitude (between -90 to 90)**

upper limit:
90 * 1e7 = 900000000 + 90*1e7 = 1800000000

mid limit:
0 * 1e7 = 10000000 + 900000000 = 910000000

lower limit:
-90 * 1e7 = -900000000 + 90*1e7 = 0




### Example of data

```
{ seq: 5, key: 'lat/1329453986', value: 'spot/ff080bcc57d57170' }
{ seq: 8, key: 'lat/1329600000', value: 'spot/ff080bcc5a99d8b8' }
{ seq: 6, key: 'long/1329453986', value: 'spot/ff080bcc57d57170' }
{ seq: 9, key: 'long/1329600000', value: 'spot/ff080bcc5a99d8b8' }
{
  seq: 4,
  key: 'spot/ff080bcc57d57170',
  value: {
    lat: 42.9453986,
    long: -2.7196453,
    species: 'Boletus Edulis',
    tags: [ 'cep', 'penny bun', 'porcini' ],
    observations: { description: 'Cep next to a path!' }
  }
}
{
  seq: 7,
  key: 'spot/ff080bcc5a99d8b8',
  value: {
    lat: 42.96,
    long: -2.728,
    species: 'Cantharelllus lutescens',
    tags: [ 'chantarelles', 'yellow foot' ],
    observations: {
      description: 'Little pine patch with yellow foot chantarelles from December to nearly February'
    }
  }
}
```


## Friday, 21st May 2021

### TODO for Today

* Interaction with the map:
  * [x] Click on marker and display information box
  * [x] Add wikipedia link for the mushies  
  * Click on the map to add new spot to local or to given guide:
    * [ ] Capture user's additional input
    * [ ] Select guide
    * [ ] Send `api/spots/add` API request to add mushrooms to the localdb
    * [ ] Reload the map with new information
  * [ ] Add edit and delete buttons on the popups
* APIs:
  * [x] get spot
  * [x] edit spot
  * [x] delete spot
* Connect to another peers' map 
  * [ ] Input for a key
  * [ ] The key is saved in localdb
  * [ ] Read the remote hypercore
  * [ ] API to subscribe to a key
  * [x] API to add guide
  * [x] API to get all guides
  * [x] API to add spots to a given guide
  * [x] API to read all spots from a guide 
  * [x] Web page to load a guide and call the API
* [x] List remote + local cores and display
* [x] Create guides:
  * A guide is one or more hypercores
  * A guide can be shared among different users
  * Although each use writes in their local feed, the guide shows like a collaborative common set
  * A user can add spots to their local feed AND/OR to their local guides



### More APIs:

Get spot info by spot ID:
``` 
curl "http://localhost:5000/api/spots/ff080bcc5cdfd2f8"
```

Edit spot:

```
# Let's edit the spot with id ff080bcc5cdfd2f8
# to fix the typo of s/Cantharelllus/Cantharellus
# and to add one more observation

curl -X POST -d '{"lat":42.96, "lng":-2.728, "tags":["chantarelles", "yellow foot"], "species": "Cantharellus lutescens", "observations":{ "description": "Little pine patch with yellow foot chantarelles from December to nearly February", "ecosystem": "Pine and bush" }}' -H "Content-Type: application/json" http://localhost:5000/api/spots/ff080bcc5cdfd2f8/edit/

# We can check the changes have been successfully done with:
curl "http://localhost:5000/api/spots/ff080bcc5cdfd2f8"

# If we edit the lat or the long, we should see how the pointers are destroyed and recreated
# For example, we should see that the pointers to ff080bcc5eee50c8 are erased and created again
curl -X POST -d '{"lat":42.9511781, "lng":-2.7350861, "tags":["chantarelles", "yellow foot"], "species": "Cantharellus lutescens", "observations":{ "description": "Loads of chanties every year." }}' -H "Content-Type: application/json" http://localhost:5000/api/spots/ff080bcc5eee50c8/edit/

# Initially:
# { seq: 12, key: 'lng/1772649140', value: 'spot/ff080bcc5eee50c8' }
# { seq: 11, key: 'lat/1329511780', value: 'spot/ff080bcc5eee50c8' }
# After update:
# { seq: 26, key: 'lng/1772649138', value: 'spot/ff080bcc5eee50c8' }
# { seq: 25, key: 'lat/1329511781', value: 'spot/ff080bcc5eee50c8' }
```

Delete spot:
```
# Let's add something bad:
curl -X POST -d '{"lat":42, "lng":-2.6, "tags":["poison", "bad mushie"], "species": "Poison Pax", "observations":{ "description": "They looked like some kind of lactarius but they are... PAX!! UO-OOOO! The poison of the universe!" }}' -H "Content-Type: application/json" http://localhost:5000/api/spots/add/
# Result is ff080bcc75ef65b0
# With three entries, one for the payload and two for lat/lng pointers

# Now let's delete ff080bcc75ef65b0
curl -X DELETE "http://localhost:5000/api/spots/ff080bcc75ef65b0"
```



### Cleanup and setup

Look at examples here:
https://hypercore-protocol.org/guides/examples/hyperbee-app/


### Guides and feeds!

Guides are collaborative feeds formed by one or more users, each one with its own Hyperbee. From the
system point of view, a feed is the same "set" of data. However, hyperbees are local append-only
logs, and only have writing rights for the user that has created it. For that reason, and to build a
"colaborative" structure, each user that collaborates with a guide, can write in a guide-specific
local hyperbee and can read the guide-specific hyperbees of the other collaborating users.



**Use case 1:** User creates a guide but doesn't share it.
* create new guide:
  * name `"my guide"` & description
  * new hyperbee is created (key + discoveryKey)
  * add guide key in localdb:
    * `guide/slugify( "my guide" )` -> {key: guideKey, createdBy: me}
    * `guide/slugify( "my guide" )/remote` -> {key: remoteReadKey, createdBy: other}
    * `guide/slugify( "my guide" )/remote` -> {key: remoteReadKey, createdBy: another}
* get guides:
  * `"my guide"`
* show spots:
  * show my local spots
  * show spots by guide `"my guide"`
* add new spot:
  * added locally
  * _can I add a spot locally AND in a guide?_
  * _can I add a spot in a guide but not have it in my local spots?_
  * copy added to guides `[ "my guide", ]`
* edit spots:
  * _shall we edit only the localdb one or all its copies?_
  * probably all its copies, the fact that this spot is shared

**User case 2:** User is invited to collaborate on a guide.
* Add guide: 
  * user needs to add the other hypercore key
  * add in localdb
    * `guide/slugify( "their guide ")/remote` -> {key: guideKey, createdBy: they}
    * _how do we know it's remote or local?_
    * _how do we know who is part of this guide?_
  * create a local hypercore and associate it with this guide
    * `guide/slugify( "their guide ")` -> {key: guideKey, createdBy: me}
  * _this needs to be shared with the other person, how???_


#### Database architecture

**Spot in only one place (localdb or guides)** Decided for this one!
* ✅ One spot belongs to one place, no need to maintain copies updated
* ❌ To see all my spots, I need to search over ALL my guides
* UNLESS: I add spot to guidedb and pointer to spot in localdb
  * `lat/<LAT>/remote` -> { guide: guideId, spot: spotId }
  * `lat/<LAT>` -> spotId
* I can move a spot from my localdb to a guide:
  * remove main key and pointers from localdb
  * save main key and pointers into guidedb
  * create remote pointers in localdb
* ✅ I can filter what spots I see, see only those from a guide, or from no guide
* ❌ Seeing all spots added by me means searching in all dbs

Spots duplicated, always in localdb
* Show all my spots (all added by me) should be fairly simple
* Show only spots from a guide, also simple
* ❌ Need to keep track of updates and deletions, and keep guides sync
* In the localdb I need to keep track of what spots are duplicated in what guides
  * `spot/<SPOT_ID>/guide` -> guideId
* ✅ Moving one spot to a guide, is just making a copy of it, and adding a key in localdb
* ✅ Seeing all spots added by me is just querying localdb


#### Guide APIs

```
# Create guide
curl -X POST -d '{"name":"The Magpie!", "description":"the wonders of the UK forests"}' -H "Content-Type: application/json" "http://localhost:5000/api/guides/new"

# Get all guides
curl "http://localhost:5000/api/guides/"

# Get one guide
curl "http://localhost:5000/api/guides/the-magpie/"
```

Now the important part, let's add a new spot into a guide:

```
curl -X POST -d '{"lat":50.857313, "lng":-1.594481, "tags":["poison", "bad mushie"], "species": "Poison Pax", "observations":{ "description": "They looked like some kind of lactarius but they are... PAX!! UO-OOOO! The poison of the universe!" }}' -H "Content-Type: application/json" http://localhost:5000/api/guides/the-magpie/spots/add/

curl -X POST -d '{"lat":50.854813, "lng":-1.585725, "tags":["king bolete", "cep", "porcini"], "species": "Boletus Edulis", "observations":{ "description": "Massive amounts of boletes near Nomansland at the beginning of the season." }}' -H "Content-Type: application/json" http://localhost:5000/api/guides/the-magpie/spots/add/
```

And inspect all the spots from a given guide:
```
curl http://lcoalhost:5000/api/guides/the-magpie/spots
```


## Saturday, 22nd May 2021

### Hyperswarm

https://github.com/hyperswarm/hyperswarm

* Interaction with the map:
  * [ ] Click on the map to add new spot to local or to given guide:
    * [ ] Capture user's additional input
    * [ ] Select guide
    * [ ] Send `api/spots/add` API request to add mushrooms to the localdb
    * [ ] Reload the map with new information
  * [ ] Add edit button on the popup
    * [ ] Can edit details from the spot (name, tags, observations)
    * [ ] _How about location? can I move it?_
    * [ ] Can move it between guides, or remove it from a guide
  * [ ] Add delete button on the popups
* Connect to another peers' map 
  * [x] API to add guide
  * [x] API to get all guides
  * [x] API to add spots to a given guide
  * [x] API to read all spots from a guide 
  * [x] Web page to load a guide and call the API
  * [ ] Web interface to invite user to guide
  * [ ] Web interface to join guide
  * [ ] Web interface to confirm joining of collaborator to guide
  * [x] API to subscribe to a key:
    * The information from the guide is in the guide's core metadata entry
    * So the only thing is the key
    * [x] POST request with key as body parameter
    * [x] I have to connect to it, and request metadata key
    * [x] From the value I take all necessary information
    * [x] I save it in my localdb with `guide/<guide slug>/<creator username>` -> { name, key }
    * [x] I create a local core with the same metadata info field
    * [x] I save a localdb entry with `guide/slug/` -> { localkey }
    * _Should we save in the metadata the users who are collaborating with it?_
      * contributors: { username: genocation, key: key, discoveryKey: discoveryKey } for example
    * _How to allow the creator to know that there's one more person in the feed and keep track of
    the other core?_ 
  * [x] API to confirm subscription to a key
  * [x] If guide is not created by me, show with a different color in web interface
  * _How do we handle guide authorship?_
* More APIs:
  * [ ] Edit guide information
  * [ ] Delete guide


### Fun tips, pipe response to jq

Jq is an awesome command-line tool to format and pretty print a JSON input.

To use it, add `-s` to the CURL to remove the download information, and pipe it through to jq. 
```
curl "http://localhost:5000/api/guides/the-magpie" -s | jq
```


### Joining Guide APIs

Create a new guide locally
```
curl -X POST -d '{"name":"Urrakas", "description":"the basque mushroom world"}' -H "Content-Type: application/json" "http://localhost:5000/api/guides/new"
```

Create user magpie:
```
curl "http://localhost:5000/api/user/set?username=magpie&emoji=%26%23128055&color=%232d2d2d"
```

Join guide:
```
curl -X POST -d '{"key": "ce533dd659fea4821dcba6d0838ef8e762b051a5d8f0645f1ec087eeaf32731f"}' -H "Content-Type: application/json" http://localhost:5000/api/guides/join/
```

Confirm guide collaboration:
```
curl -X POST -d '{"key": "73462b65f570fdb47ef797be9f429fc45e966d23b40bcbdc2ab5e6c0e87f507f"}' -H "Content-Type: application/json" http://localhost:5000/api/guides/confirm/
```

Delete guide:
```
curl -X DELETE "http://localhost:5000/api/guides/the-magpie"
```

**NOTE:** Delete guide API is incomplete, it doesn't wipe out the locally stored hypercore data.
[Documentation from
Hypercora](https://github.com/hypercore-protocol/hypercore#feeddestroystoragecallback) says that:

> ```
> feed.destroyStorage([callback])
> ```
> Destroys all stored data and fully closes this feed.
> 
> Calls the callback with (err) when all storage has been deleted and closed.

However when we try to run this with Hyperbee.feed, which returns a RemoteHypercore, it complains
saying that the RemoteHypercore doesn't implement 


### User interaction to join a collaborative guide successfully

**Starting place**
* genocation creates a guide, which is a local hypercore that she can read and write spots
* magpie doesn't have any guide

**Ending place**
* genocation has the local hypercore she started with, where she can read and write
* magpie can read genocation's hypercore
* magpie has another local hypercore with the same name as genocation's one, where he can write to
* genocation can read from magpie's hypercore


**User interaction**
* genocation goes to Home->Guides->My-Guide
* genocation clicks on "invite user to collaborate"
* system to genocation: "To add a user to this guide, give the user this private key: KEY-GENOCATION"
* _genocation shares KEY-GENOCATION with magpie_
* magpie goes to Home->Guides->Join Guide
* system to magpie: "Guide key: [] If you don't have the key, the other user should find here..." 
* magpie inputs KEY-GENOCATION:
  * magpie subscribes to guide/my-guide/genocation
  * magpie creates guide/my-guide
* magpie sees Home->Guides->My-Guide
  * createdBy: genocation (guide/my-guide -> metadata.createdBy)
  * status: confirmed/unconfirmed
    * (guide/my-guide/genocation -> metadata.contributors doesn't have magpie's key
* **Now magpie can read genocation hypercore, but genocation cannot read magpie hypercore**
  * We can disallow magpie reading genocation guide until he's shared his key with her
  * We know that he hasn't because guide/my-guide/genocation -> metadata.contributors doesn't
  include magpie's nick or discoveryKey
* system to magpie: "Warning: finish subscribing by sharing this key with genocation: KEY-MAGPIE"
* _magpie shares KEY-MAGPIE with genocation_ 
* genocation goes to Home->Guides->My-Guide
* genocation clicks on "confirm collaboration"
* system to genocation: "Add the confirmation key here: []"
* genocation inputs KEY-MAGPIE
* **Now genocation can read magpie hypercore**:
  * genocation subscribes to guide/my-guide/magpie
  * genocation updates guide/my-guide -> metadata.contributors with magpie nick or discoveryKey



### Data representations

#### Step 1: User genocation creates guide

* genocation.local.[guide/my-guide].key = FIRST_KEY_MY_GUIDE
* genocation.[guide/my-guide].metadata: 
  ```
  {
    slug: 'my-guide',
    name: 'My Guide!',
    description: 'description about this guide',
    createdOn: Date(),
    createdBy: 'genocation',  # Initial creation of guide
    writer: 'genocation',     # This database owner
    contributors: {}
  }
  ```

#### Step 2: User magpie joins guide

* magpie.local.[guide/my-guide/genocation] = FIRST_KEY_MY_GUIDE 
* magpie.local.[guide/my-guide] = MAGPIE_KEY_MY_GUIDE
* magpie.local.[guide/my-guide].metadata: 
  ```
  {
    slug: 'my-guide',
    name: 'My Guide!',
    description: 'description about this guide',
    createdOn: Date(),
    createdBy: 'genocation',  # This helps us figure out that we need to check
    writer: 'magpie'          # guide/my-guide/genocation to get contributor information
  }
  ```
* magpie.[guide/my-guide/genocation].metadata: 
  ```
  {
    slug: 'my-guide',
    name: 'My Guide!',
    description: 'description about this guide',
    createdOn: Date(),
    createdBy: 'genocation',
    writer: 'genocation',
    contributors: {}         # This is sync with genocation's main db, if contributors
  }                          # doesn't have my name needs we still need to confirm 
  ```

#### Step 3: User genocation confirms magpie participation

* genocation.local.[guide/my-guide].key = FIRST_KEY_MY_GUIDE
* genocation.local.[guide/my-guide/magpie].key = MAGPIE_KEY_MY_GUIDE 
* genocation.[guide/my-guide].metadata: 
  ```
  {
    slug: 'my-guide',
    name: 'My Guide!',
    description: 'description about this guide',
    createdOn: Date(),
    createdBy: 'genocation',
    writer: 'genocation',
    contributors: {
      magpie: 'MAGPIE_GUIDE_KEY'  # not sure
    }
  }
  ```

## Thursday, 3rd June 2021: Show and tell!

### Interesting reading material

**Local-first software: you own your data, in spite of the cloud** by Kleppmann et al.

https://martin.kleppmann.com/papers/local-first.pdf

> About the set of principles that local-first software should follow to accomplish data agency and
  sovereignty, collaboration, ubiquity and improve security, privacy and preservation of the data. A
  must-read for decentralized projects.


**Cooperative Ownership of Data without Blockchain** by Karissa McKelvey

https://www.digital-democracy.org/blog/blockchain/

> About how decentralized software can defy the status quo if it steps away from the Silicon Valley
  model for exploiting user data. Some words about blockchain, which rapidly comes to mind when
  talking about P2P, and why this is not the right tool for challenging the tech industry status
  quo.


**Decentralization Off The Self: 7 Maxims** by Eileen Wagner, Karissa McKelvey and Kelsie Nabben

https://decentpatterns.xyz/report/

> A really deep report of what the state of the art of decentralized technologies is right now and
  where we need to focus for the decentralized software to be accessible and significative to the
  users that can potentially benefit from it.


**The Decentralized Web of Hate** by Emmi Bevensee & Rebellius Data LLC

https://rebelliousdata.com/wp-content/uploads/2020/10/P2P-Hate-Report.pdf

> White supremacists are starting to use P2P technology and the decentralized community has been
  wondering how to tackle the problem, what models of authority will work with P2P, and how to
  protect P2P tools so that they can be used for positive social impact.


**Decentralization and Decency**, talk by Paul Frazee in FOSDEM 2021

https://www.youtube.com/watch?v=ULFj714_Vvo&t=693s

> Paul Frazee is one of the developers of Scuttlebutt, Beaker Browser, and currently part of
  Hyperdivision, building the Hypercore protocol. He develops CTZN, a P2P social network using
  Hypercore protocol, on live stream: https://github.com/bluelinklabs/ctzn


### Understanding Hypercore Protocol

![hypercore 1](1.jpg)
![hypercore 2](2.jpg)
![hypercore 3](3.jpg)


### How to simulate a multi-writer P2P feed

If we have two computers we can run the app in both of them to see how data replicates between them.
In case we don't, we can simulate two machines sharing a network by having one app run in our
machine and another instance running inside a container. For this, I've created a docker
configuration so that the disk space where these two save their data is not shared between host and
container.

##### 1. Run the app A in your host

Open a terminal and run in the app main directory:

```
$ npm install
$ npm start

Listening on http://localhost:5000
Hyperspace daemon started.
Seeding:
   local: 43a77c592a621d3ce82912902a6dafb7dcd2d51e79f4f26f37867b61e34a84bf
```

##### 2. Run the app B in a container

Open another terminal and run:

```
$ COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose build
$ docker-compose up

web_1  | Listening on http://localhost:3000
web_1  | Hyperspace daemon started.
web_1  | Seeding:
web_1  |    local: 233e15f0a68d4e03beffd13d948138e806c0e1a1d3809329cd8c70d36698a4b2
```

You can see what port is listening for each instance of each app, and the public key of each local
hypercore, which is the default hypercore that is in charge of saving the local data and
bootstrapping the app.


##### 3. Run a basic use case

From a third terminal you can run API requests using CURL against either the app A from the host
machine (to localhost:5000) or the app B in the container (to localhost:3000)

* App A sets their personal configuration (username: genocation)

  ```
  $ curl "http://localhost:5000/api/user/set?username=genocation&emoji=%26%23127825&color=%23c1fcac"
  ```
  Reload both apps to see how the changes reflect on app A but app B remains the same

* App B sets their personal configuration (username: magpie)

  ```
  $ curl "http://localhost:3000/api/user/set?username=magpie&emoji=%26%23128055&color=%232d2d2d"
  ```
  Reload again to see the changes applied on app B

* App A can add spots to her local DB, which will remain local and will never be shared
  ```
  $ curl -X POST -d '{"lat":42.9453986, "lng":-2.7196453, "tags":["cep", "penny bun", "porcini"], "species": "Boletus Edulis", "observations":{ "description": "Cep next to a path!" }}' -H "Content-Type: application/json" http://localhost:5000/api/spots/add/
  $ curl -X POST -d '{"lat":42.96, "lng":-2.728, "tags":["chantarelles", "yellow foot"], "species": "Cantharellus lutescens", "observations":{ "description": "Little pine patch with yellow foot chantarelles from December to nearly February" }}' -H "Content-Type: application/json" http://localhost:5000/api/spots/add/
  $ curl -X POST -d '{"lat":42.951178, "lng":-2.735086, "tags":["chantarelles"], "species": "Cantharellus tubaeformis", "observations":{ "description": "Loads of chanties every year" }}' -H "Content-Type: application/json" http://localhost:5000/api/spots/add/
  ```

* App A can create a new guide, which will allow her to share spots with other peers. To create the
guide she can use the web interface: Home->My Guides->Create and use the form to create a new guide
called **"The Magpie!"**. We can also use the following API call:
  ```
  curl -X POST -d '{"name":"The Magpie!", "description":"the wonders of the UK forests"}' -H "Content-Type: application/json" "http://localhost:5000/api/guides/new"
  ```
  If we reload the app A we can see the new guide on the bottom of the page, with some important
  details such as the number of contributors that this guide has (currently 0).

  Also, in the terminal that is running the app A in the host, we can see the private key of the new
  hypercore that has been created for this guide, being seeded. This is now connected to the swarm
  and it can be discovered and read by any other peers if they **have** the key.

* Genocation is going to add some data into the new guide.
  ```
  curl -X POST -d '{"lat":50.857313, "lng":-1.594481, "tags":["poison", "bad mushie"], "species": "Poison Pax", "observations":{ "description": "They looked like some kind of lactarius but they are... PAX!! UO-OOOO! The poison of the universe!" }}' -H "Content-Type: application/json" http://localhost:5000/api/guides/the-magpie/spots/add/

  curl -X POST -d '{"lat":50.854813, "lng":-1.585725, "tags":["king bolete", "cep", "porcini"], "species": "Boletus Edulis", "observations":{ "description": "Massive amounts of boletes near Nomansland at the beginning of the season." }}' -H "Content-Type: application/json" http://localhost:5000/api/guides/the-magpie/spots/add/
  ```
  This should reflect on the guide web interface, which is http://localhost:5000/guide/the-magpie

* Let's invite new peers! Genocation invites Magpie to join her guide.

  For this, in app A, we go to the main site, and in the thumbnail for the guide, we click on
  "invite". This will take us to a page where we can copy the invitation key and give it to our peer
  using a private and trusted channel (so, not whatsapp).

  Notice that this key is the public key of the new hypercore created when creating the new guide.

* Magpie accepts the invitation and joins the guide.

  From the browser we navigate into app B (http://localhost:3000) and we click on the "join" link at
  the bottom right. The web will ask us to add the invite key, where we will input the invitation
  key that we got privately from genocation.

  Once accepted the invitation, the terminal that shows the running log of app B will output that
  our peer hypercore is being replicated (`guide/the-magpie/genocation: <public key>`) and also a
  new hypercore has been created (`guide/the-magpie/: <public key>`).

  The shared feed now consist on two local hypercores, the original one created by genocation, and
  another one created by magpie. Each user can write in their local core, and magpie can read
  genocation's one.

  But we still need to finaliza the invite process, which is two steps

* Genocation writes another spot and it's directly streamed to the reading peers.

  We use the API to save one more spot from app A (localhost:5000):
  ```
  curl -X POST -d '{"lat":50.855, "lng":-1.59, "tags":["cantharellus", "chanterells"], "species": "Cantharellus cibarius", "observations":{ "description": "Some nice chantarelles at the beginning of September" }}' -H "Content-Type: application/json" http://localhost:5000/api/guides/the-magpie/spots/add/
  ```
  After doing this, we should see the changes when we reload app A in our browser, and because both
  peers are connected, it should have directly streamed to app B machine, so if we reload
  localhost:3000 the change should be there already.

* Magpie writes something into the guide:

  ```
  curl -X POST -d '{"lat":50.856, "lng":-1.583, "tags":["amanita muscaria", "fly agaric"], "species": "Amanita Muscaria", "observations":{ "description": "Baby amanitas found near the path." }}' -H "Content-Type: application/json" http://localhost:3000/api/guides/the-magpie/spots/add/
  ```
  If we reload app B, we will see the changes. However, this has not been streamed to app A, because
  genocation is still not a reader of magpie's hypercore.

  For having multi-writer and multi-reader, we must finish the invitation process, which is a two
  step process. 

* Genocation confirms the addition of magpie into the feed:
  
  For this, Magpie should get the public key of their hypercore and share it with genocation. For
  this, in the guide thumbnail of app B browser (localhost:3000), magpie can click on "confirm" and
  it will be redirected to a site where their public key is displayed.

  Again, magpie should transmit this key to genocation through a secure channel.

  Genocation can now go to her browser (localhost:5000) and in the-magpie thumbnail, click
  "confirm", where she will be asked to introduce the confirmation key.

  Once this is done, the terminal showing app A log should display a new key being seeded
  (`guide/the-magpie/magpie: <private key>`). Now, reloading the page from The Magpie guide should
  include the new spot that was recently added by magpie, as the data has been streamed directly
  from one machine to the other.



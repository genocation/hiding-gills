import express from 'express'
import { fileURLToPath } from 'url'
import path from 'path'
import { Liquid } from 'liquidjs'

import { filters } from './src/liquid.js'
import * as hyperspace from './src/hyperspace.js'

export const PORT = process.env.LISTENING_PORT || 5000
export const INSTALL_PATH = path.dirname(fileURLToPath(import.meta.url))

const app = express()
const engine = new Liquid()

// Engine filters
engine.registerFilter( 'count', filters.count  )

// Setup Hyperspace
hyperspace.setup();


// Config static folder
app.use(express.static(path.join(INSTALL_PATH, 'assets')))
app.use(express.json())
app.use(express.urlencoded())

// Settings
app.engine('liquid', engine.express())
app.set('views', path.join(INSTALL_PATH, 'views'))
app.set('view engine', 'liquid')

/* Web Client Routes */

/**
 * Home page
 */
app.get('/', (req, res) => {
	const allGuides = hyperspace.getGuides()
	const userInfo = hyperspace.getUserInfo()
	Promise.all( [userInfo, allGuides] ).then( ( [info, guides] ) => {
		info.guides = guides
		res.render('index', info )
	}).catch((e) => {
		// TODO figure out what went wronge
		res.render('error', { error: 'Oops! something went wrong!' })
	})
})

/**
 * Create guide page
 */
app.get('/guide/new', (req, res) => {
	res.render('create-guide')
})

app.get('/guide/join', (req, res) => {
	res.render('join-guide')
})


/**
* Guide page
 */
app.get('/guide/:slug', (req, res) => {
	const userInfo = hyperspace.getUserInfo()
	const guideInfo = hyperspace.getGuide( req.params.slug )
	Promise.all( [userInfo, guideInfo] ).then( ( [info, guide] ) => {
		info.guide = guide
		res.render('index', info)
	}).catch((e) => {
		// TODO return different errors depending on what happens
		res.render('error', { error: 'Guide not found!' })
	})
})

app.get('/guide/:slug/delete', (req, res) => {
	const userInfo = hyperspace.getUserInfo()
	const guideInfo = hyperspace.getGuide( req.params.slug )
	Promise.all( [userInfo, guideInfo] ).then( ( [info, guide] ) => {
		info.guide = guide
		res.render('delete-guide', info)
	}).catch((e) => {
		// TODO return different errors depending on what happens
		res.render('error', { error: 'Guide not found!' })
	})
})

app.get('/guide/:slug/edit', (req, res) => {
	const userInfo = hyperspace.getUserInfo()
	const guideInfo = hyperspace.getGuide( req.params.slug )
	Promise.all( [userInfo, guideInfo] ).then( ( [info, guide] ) => {
		info.guide = guide
		res.render('edit-guide', info)
	}).catch((e) => {
		// TODO return different errors depending on what happens
		res.render('error', { error: 'Guide not found!' })
	})
})

app.get('/guide/:slug/invite', (req, res) => {
	const userInfo = hyperspace.getUserInfo()
	const guideInfo = hyperspace.getGuide( req.params.slug )
	const inviteKey = hyperspace.getPublicKey( req.params.slug )
	Promise.all( [userInfo, guideInfo, inviteKey] ).then( ( [info, guide, key] ) => {
		info.guide = guide
		info.guide.key = key
		res.render('invite-key', info)
	}).catch((e) => {
		// TODO return different errors depending on what happens
		res.render('error', { error: 'Guide not found!' })
	})
})

app.get('/guide/:slug/confirm', (req, res) => {
	const userInfo = hyperspace.getUserInfo()
	const guideInfo = hyperspace.getGuide( req.params.slug )
	const confirmKey = hyperspace.getPublicKey( req.params.slug )
	Promise.all( [userInfo, guideInfo, confirmKey] ).then( ( [info, guide, key] ) => {
		info.guide = guide
		if (guide.createdBy === info.username) {
			// If the guide is mine, show form to input remote confirmation key
			res.render('confirm-guide', info)
		} else {
			// If the guide is not mine, show my public key to request confirmation
			info.guide.key = key
			res.render('confirm-key', info)
		}
	}).catch((e) => {
		// TODO return different errors depending on what happens
		res.render('error', { error: 'Guide not found!' })
	})
})


/**
 * API Routes
 * TODO: extract all api to src/api.js and leave app.js only for web routes
 */

/**
 * USERS
 */

app.get('/api/user', (req, res) => {
	hyperspace.getUserInfo().then( info => {
		res.json(info)
	})
})

app.get('/api/user/set', (req, res) => {
	const parameters = req.query
	hyperspace.setUserInfo( parameters ).then(() => {
		res.sendStatus( 200 )
	}).catch(() => {
		res.sendStatus( 500 )
	})
})



/**
 * SPOTS
 */

app.post('/api/spots/add', (req, res) => {
	// TODO: validation, check that necessary values are present
	const { lat, lng, species, tags, observations } = req.body
	hyperspace.addSpotToLocal( { lat, lng, species, tags, observations } ).then(() => {
		res.sendStatus(200)
	}).catch(()=> {
		res.status(500).json({ error: 'Error adding spot' })
	})
})

app.get('/api/spots/local', (req, res) => {
	if (
		req.query.xlat &&
		req.query.ylat &&
		req.query.xlng &&
		req.query.ylng
	) {
		hyperspace.getBoundSpots( req.query ).then( spots => {
			res.json(spots)
		})
	} else {
		hyperspace.getLocalSpots().then(spots => {
			res.json(spots)
		})
	}
})

app.get('/api/spots/:id', (req, res) => {
	hyperspace.getSpot( req.params.id ).then(spot => {
		res.json(spot)
	})
	/* TODO: add error not found */
})

app.post('/api/spots/:id/edit', (req, res) => {
	// TODO: validation, check that necessary values are present
	const { lat, lng, species, tags, observations } = req.body
	hyperspace.editSpot( req.params.id, { lat, lng, species, tags, observations } ).then(() => {
		res.sendStatus(200)
	}).catch(( e )=> {
		console.log(e)
		res.status(500).json({ error: 'Error adding spot' })
	})
})

/* TODO: shall we use REST DELETE method or GET /api/spots/:id/delete ? */
app.delete('/api/spots/:id', (req, res) => {
	hyperspace.deleteSpot( req.params.id ).then(() => {
		res.sendStatus(200)
	})
	// TODO: catch deletion error
})



/**
 * GUIDES
 */

/* Guides are mixed feeds by different users. TODO in the future */
app.get('/api/guides', (req, res) => {
	/* TODO: add search parameters to filter guides by name? */
	hyperspace.getGuides().then(guides => {
		res.json(guides)
	})
})

app.post('/api/guides/new', hyperspace.identified, (req, res) => {
	const { name, description } = req.body
	if (!name) {
		res.status(500).json({ error: 'Must specify a guide name' })
		return
	}
	// TODO disallow guide creation when I have no username
	hyperspace.addGuide( name, description ).then(() => {
		res.sendStatus(200)
	})
})

app.post('/api/guides/join', hyperspace.identified, (req, res) => {
	const key = req.body.key
	if (!key) {
		res.status(500).json({ error: 'Must specify a public key' })
		return
	}
	// TODO disallow joining guide when I have no username
	hyperspace.joinGuide( key ).then(() => {
		res.sendStatus(200)
	})
	// TODO handle wrong key exception
})

// TODO add ['join', 'confirm' and 'new'] to list of disallowed slugs
app.post('/api/guides/confirm', hyperspace.identified, (req, res) => {
	const key = req.body.key
	if (!key) {
		res.status(500).json({ error: 'Must specify a public key' })
		return
	}
	hyperspace.confirmGuide( key ).then(() => {
		res.sendStatus(200)
	})
})

app.get('/api/guides/:slug', (req, res) => {
	hyperspace.getGuide( req.params.slug ).then( guide => {
		res.json(guide)
	})
	// TODO handle not found
})

/* TODO: shall we use REST DELETE method or GET /api/spots/:id/delete ? */
app.delete('/api/guides/:slug', (req, res) => {
	hyperspace.deleteGuide( req.params.slug ).then(() => {
		res.sendStatus(200)
	})
	// TODO: catch deletion error
})

app.post('/api/guides/:slug/edit', (req, res) => {
	const { name, description } = req.body
	if (!name) {
		res.status(500).json({ error: 'Must specify a guide name' })
		return
	}
	hyperspace.editGuide( req.params.slug, name, description ).then(() => {
		res.sendStatus(200)
	})
	// TODO: catch edit error
})


app.get('/api/guides/:slug/spots', (req, res) => {
	if (
		req.query.xlat &&
		req.query.ylat &&
		req.query.xlng &&
		req.query.ylng
	) {
		hyperspace.getBoundSpots( req.query, req.params.slug ).then( spots => {
			res.json(spots)
		})
	} else {
		hyperspace.getGuideSpots( req.params.slug ).then( spots => {
			res.json(spots)
		})
	}
	// TODO handle not found
})

app.post('/api/guides/:slug/spots/add', (req, res) => {
	// TODO: validation, check that necessary values are present
	const { lat, lng, species, tags, observations } = req.body
	hyperspace.addSpotToGuide( { lat, lng, species, tags, observations }, req.params.slug ).then(() => {
		res.sendStatus(200)
	}).catch((e)=> {
		res.status(500).json({ error: 'Error adding spot' })
	})
})



/**
 * POST Create a new hypercore with a given store name
 */
// app.post('/api/store/new', (req, res) => {
// 	const storeName = req.query.name
// 	hyperspace.newStore( storeName ).then( key => {
// 		res.send(key)
// 	})
// })
//
// app.post('/api/store/:key/put', (req, res) => {
// 	const body = req.body
// 	const params = req.params
// 	console.log(body)
// 	console.log(params)
// 	res.sendStatus(200)
// })



/* TEST API, print all local hyperbee entries */
app.get('/api/inspect', (req, res) => {
	hyperspace.inspect().then(() => {
		res.sendStatus(200)
	})
})


/* ERROR PAGES */
app.get('*', (req, res) => {
	res.render('error', { error: '404: Page not Found' })
})


app.listen(PORT, () => {
	console.log(`Listening on http://localhost:${PORT}`)
})



/* Cleanup routine, closes Hyperspace client and server */
process.stdin.resume()

// Fo app specific cleaning before exiting
process.on('exit', async function () {
	process.emit('cleanup')
})

// Catch ctrl+c event and exit normally
process.on('SIGINT', async function () {
	console.log('\nTerminated!')
	await hyperspace.cleanup()
	process.exit(2)
})

// Catch uncaught exceptions, trace, then exit normally
process.on('uncaughtException', async function(e) {
	console.log('\nUncaught Exception!')
	console.log(e.stack)
	await hyperspace.cleanup()
	process.exit(99)
})
